package sbu.cs;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AppTest {

    private static App instance;

    @BeforeAll
    static void setup() {
        instance = new App();
    }

    @Test
    void main1() {
        int n = 8;
        int[][] arr = {
                {2, 5, 5, 4, 2, 1, 5, 5},
                {2, 1, 2, 4, 4, 1, 5, 4},
                {4, 4, 1, 1, 1, 5, 1, 4},
                {4, 1, 4, 4, 1, 4, 5, 1},
                {1, 1, 1, 5, 1, 4, 4, 5},
                {4, 4, 5, 4, 5, 1, 5, 5},
                {1, 4, 4, 4, 1, 1, 1, 4},
                {4, 5, 4, 5, 5, 1, 4, 4}
        };
        String input = "qmiqwnhwnrckeirepjgv";
        assertEquals(instance.main(n, arr, input), "vemehewewjijejzjznenancncryrgrlrljjjpjljldedvdtdtmomimumu" +
                "sdsssosodldcdzdzmdmhmvmzizikizizxzxhxzxzpzptpzpzvzvdvzvzrzrirzrzizimizizvzvkvzvzkzktkzkzqzqwqzqztzto" +
                "tzt");
    }

    @Test
    void main2() {
        int n = 7;
        int[][] arr = {
                {3, 5, 5, 4, 3, 1, 4},
                {1, 1, 2, 4, 3, 1, 2},
                {1, 4, 3, 1, 5, 5, 1},
                {2, 1, 4, 3, 3, 4, 5},
                {1, 1, 1, 5, 1, 4, 4},
                {4, 2, 2, 4, 3, 5, 5},
                {1, 4, 4, 4, 4, 2, 1}
        };
        String input = "gfdkjssdf";
        assertEquals(instance.main(n, arr, input), "gnfmfmdndnkqkqjjjjsksksbsbddddfsfsgqgqfpfpdbdbkdkdjwjwsxsxsoso" +
                "dodofdfdgbshjqkqdqfqgqfqdqsqspjpkpdpfpgpfpdpswswjwkwdwfwgwfwdususujukudufuguftdtsttttttuuuuuuuuwwwwwwww" +
                "hhhhhhhhhhhhhhhhqqqqqqqqppppppppwwwwwwwwuuuuuuuuttttttttuuuuuuuuwwwwwwwwhhhhhhhhhhhhhhhhqqqqqqqqppppp" +
                "pppwwwwwwwwuuuuuuuuttttttttuuuuuuuuwwwwwwwwhhhhhhhhhhhhhhhhqqqqqqqqppppppppwwwwwwwwuuuuuuuuttt");
    }

    @Test
    void main3() {
        int n = 3;
        int[][] arr = {
                {3, 5, 5},
                {1, 1, 2},
                {1, 4, 3}
        };
        String input = "hello";
        assertEquals(instance.main(n, arr, input), "hheelllloohheellllooolleholleh");
    }



}
