package sbu.cs;

import org.junit.jupiter.api.Test;
import sbu.cs.machine.functions.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WhiteFunctionsTest {
    @Test
    void White1() {
        String input1 = "hello";
        String input2 = "";
        assertEquals(new White1().func(input1 , input2), "hello");
    }
    @Test
    void White2() {
        String input1 = "lkjasflkjasdf";
        String input2 = "erwrrweorekrteldsaf";
        assertEquals(new White2().func(input1 , input2), "lkjasflkjasdffasdletrkeroewrrwre");
    }
    @Test
    void White2_1() {
        String input1 = "";
        String input2 = "";
        assertEquals(new White2().func(input1 , input2), "");
    }
    @Test
    void White3() {
        String input1 = "ajfgjfjs";
        String input2 = "xbsherthsdgsdfhb";
        assertEquals(new White3().func(input1 , input2), "abjhffgdjsfgjdsshtrehsbx");
    }
    @Test
    void White3_1() {
        String input1 = "a";
        String input2 = "z";
        assertEquals(new White3().func(input1 , input2), "az");
    }
    @Test
    void White4() {
        String input1 = "";
        String input2 = "klkljjfedcbajjklkll";
        assertEquals(new White4().func(input1 , input2), "");
    }
    @Test
    void White4_1() {
        String input1 = "noteven";
        String input2 = "designated";
        assertEquals(new White4().func(input1 , input2), "designated");
    }
    @Test
    void White5() {
        String input1 = "aaaaaaaakkkkkkkkkaaaaaaaaaa";
        String input2 = "ccbbbbabbbbbcc";
        assertEquals(new White5().func(input1 , input2), "ccbbbbabllllmmkkkaaaaaaaaaa");
    }
    @Test
    void White5_1() {
        String input1 = "";
        String input2 = "b";
        assertEquals(new White5().func(input1 , input2), "b");
    }
}
