package sbu.cs;

import org.junit.jupiter.api.Test;
import sbu.cs.machine.functions.*;

import static org.junit.jupiter.api.Assertions.*;
class BlackFunctionsTest{
    @Test
    void Black1() {
        String input = "";
        assertEquals(new Black1().func(input), "");
    }
    @Test
    void Black1_2() {
        String input = "aaa";
        assertEquals(new Black1().func(input), "aaa");
    }
    @Test
    void Black2() {
        String input = "abcd";
        assertEquals(new Black2().func(input), "aabbccdd");
    }
    @Test
    void Black2_1() {
        String input = "Q";
        assertEquals(new Black2().func(input), "QQ");
    }
    @Test
    void Black3() {
        String input = "KkLkk";
        assertEquals(new Black3().func(input), "KkLkkKkLkk");
    }
    @Test
    void Black3_1() {
        String input = "";
        assertEquals(new Black3().func(input), "");
    }
    @Test
    void Black4() {
        String input = "baaLLLaab";
        assertEquals(new Black4().func(input), "bbaaLLLaa");
    }
    @Test
    void Black5() {
        String input = "zyxwvutsrqponmlkjihgfedcba";
        assertEquals(new Black5().func(input), "abcdefghijklmnopqrstuvwxyz");
    }
    @Test
    void Black5_1() {
        String input = "";
        assertEquals(new Black5().func(input), "");
    }
    @Test
    void Black5_2() {
        String input = "AbCxYz";
        assertEquals(new Black5().func(input), "ZyXcBa");
    }

}