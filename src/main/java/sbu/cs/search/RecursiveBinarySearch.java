package sbu.cs.search;

public class RecursiveBinarySearch implements Searching{
    public int searchingMethod(int[] arr, int size)  {
        int l = 0 ;
        int r = arr.length - 1 ;
        return recursiveBinarySearch(arr, 0, arr.length - 1,size);
    }
    public int recursiveBinarySearch(int arr[], int l, int r, int x)
    {
        if (r >= l) {
            int mid = l + (r - l) / 2;

            if (arr[mid] == x)
                return mid;

            if (arr[mid] > x)
                return recursiveBinarySearch(arr, l, mid - 1, x);


            return recursiveBinarySearch(arr, mid + 1, r, x);
        }

        return -1;
    }

}
