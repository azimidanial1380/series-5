package sbu.cs.machine;

import sbu.cs.machine.colors.*;

public class MagicMachine {

    Box[][] skeleton;
    int size;

    public MagicMachine(int size, int[][] numbers) {
        if (size < 3) {
            throw new RuntimeException("ERROR : size must be 3 or more !");
        }

        this.size = size;
        skeleton = new Box[size][size];

        House[] downOutputs = new House[size];
        downOutputs(downOutputs);

        FirstRow(downOutputs, numbers);
        MiddleRows(downOutputs, numbers);
        LastRow(downOutputs, numbers);
    }

    private void FirstRow(House[] downOutputs, int[][] numbers) {
        House rightOut = new House();
        skeleton[0][0] = new MachineInput(new House(), rightOut, downOutputs[0]
                , numbers[0][0]);

        for (int i = 1; i < size - 1; i++){
            House input = rightOut;
            rightOut = new House();
            skeleton[0][i] = new GreenBox(input, rightOut, downOutputs[i]
                    , numbers[0][i]);
        }

        skeleton[0][size - 1] = new FlaxBox(rightOut, downOutputs[size - 1]
                , numbers[0][size - 1]);
    }

    private void MiddleRows(House[] upInputs, int[][] numbers) {
        House[] downOutputs = new House[size];

        for (int i = 1; i < size - 1; i++){
            downOutputs(downOutputs);
            House rightOut = new House();
            skeleton[i][0] = new GreenBox(upInputs[0], rightOut, downOutputs[0]
                    , numbers[i][0]);

            for (int j = 1; j < size - 1; j++){
                House input = rightOut;
                rightOut = new House();
                skeleton[i][j] = new AquaBox(input, upInputs[j], rightOut
                        , downOutputs[j], numbers[i][j]);
            }

            skeleton[i][size - 1] = new PinkBox(rightOut, upInputs[size - 1]
                    , downOutputs[size - 1], numbers[i][size - 1]);

            System.arraycopy(downOutputs, 0, upInputs, 0, size);
        }
    }

    private void LastRow(House[] upInputs, int[][] numbers) {
        House rightOut = new House();
        skeleton[size - 1][0] = new FlaxBox(upInputs[0], rightOut
                , numbers[size - 1][0]);

        for (int i = 1; i < size - 1; i++){
            House input = rightOut;
            rightOut = new House();
            skeleton[size - 1][i] = new PinkBox(input, upInputs[i], rightOut
                    , numbers[size - 1][i]);
        }

        skeleton[size - 1][size - 1] = new MachineOutput(rightOut, upInputs[size - 1], new House()
                , numbers[size - 1][size - 1]);
    }

    private void downOutputs(House[] downOutputs){
        for(int i = 0; i < size; i++){
            downOutputs[i] = new House();
        }
    }

    public String eval(String input){
        ((MachineInput)skeleton[0][0]).setInput(input);
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                skeleton[i][j].eval();
            }
        }
        return ((MachineOutput)skeleton[size - 1][size - 1]).getOutput();
    }
}

