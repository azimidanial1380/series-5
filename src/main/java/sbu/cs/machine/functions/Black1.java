package sbu.cs.machine.functions;

public class Black1 implements BlackFunction{
    public String func(String arg) {
        String reversedStr = "";
        for (int i = arg.length() - 1; i >= 0; i--) {
            reversedStr += arg.charAt(i);
        }
        return reversedStr;
    }
}
