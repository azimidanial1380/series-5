package sbu.cs.machine.functions;

public class White2 implements WhiteFunction{
    public String func(String arg1, String arg2){
        String appendedStr = arg1 ;
        Black1 b1 = new Black1() ;
        appendedStr += b1.func(arg2) ;            // ***
        return appendedStr ;
    }
}
