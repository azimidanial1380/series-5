package sbu.cs.machine.functions;

public class BlackFunctions {
    public static String eval(int number, String input){
        switch (number) {
            case 1:
                Black1 b1 = new Black1() ;
                return b1.func(input);
            case 2:
                Black2 b2 = new Black2() ;
                return b2.func(input);
            case 3:
                Black3 b3 = new Black3() ;
                return b3.func(input);
            case 4:
                Black4 b4 = new Black4() ;
                return b4.func(input);
            case 5:
                Black5 b5 = new Black5() ;
                return b5.func(input);
            default:
                throw new RuntimeException("eval");
        }
    }
}
