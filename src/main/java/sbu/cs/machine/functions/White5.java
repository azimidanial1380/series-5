package sbu.cs.machine.functions;

public class White5 implements WhiteFunction{
    public String func(String arg1, String arg2){

            StringBuilder res = new StringBuilder();
            int min = Math.min(arg1.length(), arg2.length());
            for(int i = 0; i < min; i++)
                res.append((char)((arg1.charAt(i) + arg2.charAt(i) - 2 * 'a') % 26
                        + 'a'));
            res.append(arg1.substring(min)).append(arg2.substring(min));
            return res.toString();

    }

}
