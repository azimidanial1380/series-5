package sbu.cs.machine.functions;

public class Black4 implements BlackFunction{
    public String func(String arg) {
        String shiftedStr = "";
        shiftedStr += arg.charAt(arg.length() - 1);
        shiftedStr += arg.substring(0, arg.length() - 1);
        return shiftedStr;
    }
}
