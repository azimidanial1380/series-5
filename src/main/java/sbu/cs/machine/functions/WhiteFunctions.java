package sbu.cs.machine.functions;

public class WhiteFunctions {
    public static String eval(int number, String input1, String input2){
        switch (number) {
            case 1:
                White1 w1 = new White1();
                return w1.func(input1, input2);
            case 2:
                White2 w2 = new White2();
                return w2.func(input1, input2);
            case 3:
                White3 w3 = new White3();
                return w3.func(input1, input2);
            case 4:
                White4 w4 = new White4();
                return w4.func(input1, input2);
            case 5:
                White5 w5 = new White5();
                return w5.func(input1, input2);
            default:
                throw new RuntimeException("number isn't valid!");
        }
    }
}

