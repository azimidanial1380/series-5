package sbu.cs.machine.functions;

public class Black5 implements BlackFunction{
    public String func(String arg) {

        String text = arg ;
        int[] chars = text.chars()
                .map(ch -> Character.isUpperCase(ch) ? 25 - ch + 'A' * 2 :
                    Character.isLowerCase(ch) ? 25 - ch + 'a' * 2 : ch)
                .toArray();
        text = new String(chars, 0, chars.length);
        return text ;
    }

}
