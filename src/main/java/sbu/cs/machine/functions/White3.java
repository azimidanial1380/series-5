package sbu.cs.machine.functions;

public class White3 implements WhiteFunction{
    public String func(String arg1, String arg2){
        Black1 b1 = new Black1() ;
        String reversedStr2 = b1.func(arg2) ;     // ***
        White1 w1 = new White1() ;                // ***
        return w1.func(arg1, reversedStr2);
    }
}
