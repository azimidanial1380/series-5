package sbu.cs.machine.functions;

public class White4 implements WhiteFunction{
    public String func(String arg1, String arg2){
        if(arg1.length()%2==0){
            return arg1 ;
        }
        return arg2 ;
    }
}
