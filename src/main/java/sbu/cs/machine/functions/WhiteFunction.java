package sbu.cs.machine.functions;

public interface WhiteFunction {
    String func(String arg1, String arg2);
}
