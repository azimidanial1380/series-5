package sbu.cs.machine.colors;

import sbu.cs.machine.*;
import sbu.cs.machine.functions.*;

public class GreenBox implements Box{
    protected House input;
    protected House rightOutput;
    protected House downOutput;
    protected final int number;

    public GreenBox(House input, House rightOutput, House downOutput, int number) {
        this.input = input;
        this.rightOutput = rightOutput;
        this.downOutput = downOutput;
        this.number = number;
    }

    @Override
    public void eval() {

        rightOutput.setHouse(BlackFunctions.eval(number, input.getHouse()));
        downOutput.setHouse(rightOutput.getHouse());

    }

}
