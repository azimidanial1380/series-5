package sbu.cs.machine.colors;
import sbu.cs.machine.House;
import sbu.cs.machine.functions.*;

public class AquaBox implements Box{
    protected House leftInput;
    protected House upInput;
    protected House rightOutput;
    protected House downOutput;
    protected final int number;

    public AquaBox(House leftInput, House upInput, House rightOutput
            , House downOutput, int number) {
        this.leftInput = leftInput;
        this.upInput = upInput;
        this.rightOutput = rightOutput;
        this.downOutput = downOutput;
        this.number = number;
    }

    @Override
    public void eval() {

        rightOutput.setHouse(BlackFunctions.eval(number, leftInput.getHouse()));
        downOutput.setHouse(BlackFunctions.eval(number, upInput.getHouse()));

    }
}
