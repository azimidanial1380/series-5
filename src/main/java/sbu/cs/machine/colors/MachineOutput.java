package sbu.cs.machine.colors;

import sbu.cs.machine.House;

public class MachineOutput extends PinkBox{

        public MachineOutput(House leftInput, House upInput, House output, int number) {
            super(leftInput, upInput, output, number);
        }

        public String getOutput(){
            return this.output.getHouse();
        }

}
