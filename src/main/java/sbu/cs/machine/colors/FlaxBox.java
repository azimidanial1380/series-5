package sbu.cs.machine.colors;

import sbu.cs.machine.*;
import sbu.cs.machine.functions.BlackFunctions;

public class FlaxBox implements Box {
    protected House input;
    protected House output;
    protected final int number;

    public FlaxBox(House input, House output, int number) {
        this.input = input;
        this.output = output;
        this.number = number;
    }

    @Override
    public void eval() {

        output.setHouse(BlackFunctions.eval(number, input.getHouse()));

    }
}
