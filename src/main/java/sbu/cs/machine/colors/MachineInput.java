package sbu.cs.machine.colors;


import sbu.cs.machine.House;

public class MachineInput extends GreenBox {
    public MachineInput(House input, House rightOutput, House downOutput, int number) {
        super(input, rightOutput, downOutput, number);
    }

    public void setInput(String input){
        this.input.setHouse(input);
    }
}
