package sbu.cs.machine.colors;

import sbu.cs.machine.functions.*;
import sbu.cs.machine.*;

public class PinkBox implements Box{
    protected House leftInput;
    protected House upInput;
    protected House output;
    protected final int number;

    public PinkBox(House leftInput, House upInput, House output, int number) {
        this.leftInput = leftInput;
        this.upInput = upInput;
        this.output = output;
        this.number = number;
    }

    @Override
    public void eval() {

        output.setHouse(WhiteFunctions.eval(number, leftInput.getHouse()
                , upInput.getHouse()));

    }

}


