package sbu.cs;

import sbu.cs.search.IterativeBinarySearch;
import sbu.cs.search.RecursiveBinarySearch;
import sbu.cs.sort.InsertionSort;
import sbu.cs.sort.MergeSort;
import sbu.cs.sort.SelectionSort;

public class SortArray {

    /**
     * sbu.cs.sort array arr with selection sbu.cs.sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] selectionSort(int[] arr, int size) {
        SelectionSort obj = new SelectionSort() ;
        return obj.sortingMethod(arr, size) ;
    }

    /**
     * sbu.cs.sort array arr with insertion sbu.cs.sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] insertionSort(int[] arr, int size) {
        InsertionSort obj = new InsertionSort();
        return obj.sortingMethod(arr, size) ;
    }

    /**
     * sbu.cs.sort array arr with merge sbu.cs.sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */

    public int[] mergeSort(int[] arr, int size) {
        MergeSort obj = new MergeSort() ;
        return obj.sortingMethod(arr, size) ;
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in iterative form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearch(int[] arr, int value) {
        IterativeBinarySearch obj = new IterativeBinarySearch();
        return obj.searchingMethod(arr, value);
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in recursive form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearchRecursive(int[] arr, int value) {
        RecursiveBinarySearch obj = new RecursiveBinarySearch();
        return obj.searchingMethod(arr,value);
    }
}
