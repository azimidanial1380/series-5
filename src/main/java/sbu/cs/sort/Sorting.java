package sbu.cs.sort;

public interface Sorting {
    int[] sortingMethod(int[] arr, int size) ;
}
