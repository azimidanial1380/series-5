package sbu.cs.sort;

public class SelectionSort implements Sorting {
    public int[] sortingMethod(int[] arr, int size) {
        int []sortArr = new int[size];
        for(int i = 0; i < size; i++){
            sortArr[i] = arr[i] ;
        }

        // sorting
        for(int i = 0 ; i < size-1 ; i++){
            int minIndex = i ;
            int j  ;
            for( j = i+1 ; j < size ; j++){
                if(sortArr[minIndex]>sortArr[j]){
                    minIndex = j ;
                }
            }
            int temp = sortArr[minIndex] ;
            sortArr[minIndex] = sortArr[i] ;
            sortArr[i] = temp ;
        }

        return sortArr;
    }


}
