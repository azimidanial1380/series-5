package sbu.cs;

import sbu.cs.machine.*;

public class App {

    public String main(int n, int[][] arr, String input)
    {
        MagicMachine m = new MagicMachine(n, arr);
        String result = m.eval(input);
        return result;
    }

}
